from flask import Flask, render_template,request,redirect,url_for # For flask implementation    
from bson import ObjectId # For ObjectId to work    
from pymongo import MongoClient    
import os    
    
app = Flask(__name__)    
title = "TODO sample application with Flask and MongoDB"    
heading = "  "   
    
client = MongoClient("mongodb://127.0.0.1:27017") #host uri    
db = client.PPAP_OEE    #Select the database    
todos = db.production_orders	 #Select the collection name    
    
def redirect_url():    
    return request.args.get('next') 
  
@app.route("/list")    
def lists ():    
    #Display the all Tasks    
    todos_l = todos.find()    
    a1="active"    
    return render_template('mongo_index.html',a1=a1,todos=todos_l,t=title,h=heading)    
  


@app.route("/")    
@app.route("/uncompleted")    
def tasks ():    
    #Display the Uncompleted Tasks    
    todos_l = todos.find({"done":"no"})    
    a2="active"    
    return render_template('mongo_index.html',a2=a2,todos=todos_l,t=title,h=heading) 




@app.route("/dbrecord")    
def records ():    
    #Display the all Tasks    
    todos_l = todos.find({ 'DATE':{'$gt':"22-sep-2020"}})    
    a3="active"    
    return render_template('mongo_index2.html',a3=a3,todos=todos_l,t=title,h=heading)    

   
  
  

    
if __name__ == "__main__":    
    
    app.run()
